package de.hsb.simon.client.net;

import de.hsb.simon.client.ui.gui.EShopClientGUI;
import de.hsb.simon.client.ui.gui.comp.kundenMenue.artikel.KundenArtikelListePanel;
import de.hsb.simon.client.ui.gui.comp.kundenMenue.rechnung.KundenRechnungsListePanel;
import de.hsb.simon.client.ui.gui.comp.mitarbeiterMenue.artikel.MitarbeiterArtikelListePanel;
import de.hsb.simon.client.ui.gui.comp.mitarbeiterMenue.kunden.MitarbeiterKundenListePanel;
import de.hsb.simon.client.ui.gui.comp.mitarbeiterMenue.mitarbeiter.MitarbeiterMitarbeiterListePanel;
import de.hsb.simon.client.ui.gui.comp.mitarbeiterMenue.rechnung.MitarbeiterRechnungsListePanel;
import de.hsb.simon.commons.ClientInterface;
import de.hsb.simon.commons.ServerInterface;
import de.hsb.simon.commons.SessionInterface;
import de.root1.simon.Lookup;
import de.root1.simon.Simon;
import de.root1.simon.annotation.SimonRemote;
import de.root1.simon.exceptions.EstablishConnectionFailed;
import de.root1.simon.exceptions.LookupFailedException;
import valueobjects.Artikel;
import valueobjects.Kunde;
import valueobjects.Mitarbeiter;
import valueobjects.Rechnung;

import java.net.UnknownHostException;
import java.util.HashMap;

@SimonRemote( value = {ClientInterface.class})
public class ClientInterfaceImpl implements ClientInterface{

    private Lookup lookup;
    private ServerInterface server;
    private SessionInterface session;
    private boolean secureLogout = false;

    private EShopClientGUI gui;
    private MitarbeiterArtikelListePanel mArtList;

    public ClientInterfaceImpl() {
    }



    /**
     * Speichert die zugehörige GUI in einem Attribut, damit deren Werte aktualisiert werden können
     *
     * @param gui -> Die zugehörige GUI
     */
    public void initializeGUI(EShopClientGUI gui) {
        this.gui = gui;
    }

    public void callback(String text){
        System.out.println(text);
    }

    /**
     * Übermittelt der GUI ein neues MitArbeiterArtikelListePanel mit den neuen Werten
     *
     * @param neueDaten -> Die Hashmap mit dem neuen Artikelbestand
     */
    public void receiveNeueArtikelDaten(HashMap<Integer,Artikel> neueDaten) {
        this.gui.setAddKundenArtikelListePanel(new KundenArtikelListePanel(neueDaten));
        this.gui.setAddMitarbeiterArtikelListePanel(new MitarbeiterArtikelListePanel(neueDaten));
        this.gui.switchPanelRepainter();
        this.gui.initListeners();
    }

    public void receiveNeueKundenDaten(HashMap<Integer, Kunde> neueDaten){
        this.gui.setAddMitarbeiterKundenListePanel(new MitarbeiterKundenListePanel(neueDaten));
        this.gui.switchPanelRepainter();
        this.gui.initListeners();
    }

    public void receiveNeueMitarbeiterDaten(HashMap<Integer, Mitarbeiter> neueDaten){
        this.gui.setAddMitarbeiterMitarbeiterListePanel(new MitarbeiterMitarbeiterListePanel(neueDaten));
        this.gui.switchPanelRepainter();
        this.gui.initListeners();
    }

    public void receiveNeueRechnungsDaten(HashMap<Integer, Rechnung> neueDaten){
        this.gui.setAddMitarbeiterRechnungsListePanel(new MitarbeiterRechnungsListePanel(neueDaten));
        this.gui.switchPanelRepainter();
        this.gui.initListeners();
    }

    public void unreferenced(){
        if(!this.secureLogout){
            System.err.println("Server is not available.");
        }
    }

    public void connectClientToServer() throws UnknownHostException, LookupFailedException, EstablishConnectionFailed{
        lookup = Simon.createNameLookup("127.0.0.1", 4753);

        server = (ServerInterface) lookup.lookup("myServer");

        session = server.login("User", this);
    }

    public void sendHello(){
        session.sendMessage("Hallo!");
    }

    public void logout(){
        this.secureLogout = true;
        lookup.release(server);
    }

    public SessionInterface getSession() {
        return this.session;
    }
}
