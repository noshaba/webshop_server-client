package de.hsb.simon.client.ui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.UnknownHostException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import de.hsb.simon.client.net.ClientInterfaceImpl;
import de.root1.simon.exceptions.EstablishConnectionFailed;
import de.root1.simon.exceptions.LookupFailedException;

public class ClientGUI extends JFrame {
	
	private static final long serialVersionUID = 8996112056079575641L;

	private JTextArea area;
	private JButton button;
	
	private ClientInterfaceImpl connection;
	
	public ClientGUI() {
		super("Simon Client");
		
		connection = new ClientInterfaceImpl();
		
		this.setLayout(new BorderLayout());
		this.setSize(400, 300);
		
		area = new JTextArea();
		area.setEditable(false);
		
		button = new JButton("Sende ein Hallo");
		
		this.add(new JScrollPane(area), BorderLayout.CENTER);
		this.add(button, BorderLayout.SOUTH);
		
		button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                connection.sendHello();
            }
        });

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);    //To change body of overridden methods use File | Settings | File Templates.
                connection.logout();
                System.exit(0);
            }
        });
		
		this.setVisible(true);

        try{

            connection.connectClientToServer();

        } catch (UnknownHostException ue){
            ue.printStackTrace();
        } catch (LookupFailedException le){
            le.printStackTrace();
        } catch (EstablishConnectionFailed ex){
            ex.printStackTrace();
        }
	}

	public static void main(String[] args) throws ClassNotFoundException,
			InstantiationException, IllegalAccessException,
			UnsupportedLookAndFeelException {
		
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		new ClientGUI();
	}
}
