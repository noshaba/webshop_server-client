package de.hsb.simon.commons;

import java.io.Serializable;
import java.util.HashMap;

import de.root1.simon.SimonUnreferenced;
import valueobjects.Artikel;
import valueobjects.Kunde;
import valueobjects.Mitarbeiter;
import valueobjects.Rechnung;

public interface ClientInterface extends Serializable, SimonUnreferenced {

    public void callback(String message);

    public void receiveNeueArtikelDaten(HashMap<Integer,Artikel> neueDaten);

    public void receiveNeueKundenDaten(HashMap<Integer, Kunde> neueDaten);

    public void receiveNeueMitarbeiterDaten(HashMap<Integer, Mitarbeiter> neueDaten);

    public void receiveNeueRechnungsDaten(HashMap<Integer, Rechnung> neueDaten);

}
