package de.hsb.simon.commons;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Vector;

import de.root1.simon.SimonUnreferenced;
import exception.*;
import valueobjects.*;

public interface SessionInterface extends Serializable, SimonUnreferenced {

    public void sendMessage(String message);

    public ClientInterface getClient();

    public Vector gibAlleArtikel();

    public HashMap<Integer,Artikel> gibAlleArtikelHashMapZurueckgeben();

    public Vector gibAlleKunden();

    public HashMap<Integer,Kunde> gibAlleKundenHashMapZurueckgeben();

    public Vector gibAlleRechnungen();

    public HashMap<Integer,Rechnung> gibAlleRechnungenHashMapZurueckgeben();

    public Vector giballeRechnungenEinesKundenZurueckgeben(int kNr) throws RechnungKeineVorhandenException;

    public Vector gibAlleMitarbeiter();

    public HashMap<Integer,Mitarbeiter> gibAlleMitarbeiterHashMapZurueckgeben();

    public boolean findeMitarbeiter(String bName, String bPasswort);

    public int getMitarbeiterNr(String benutzername) throws MitarbeiterExistiertNichtException;

    public boolean findeKunden(String bName, String bPasswort);

    public Vector sucheNachName(String name);

    public Rechnung sucheNachRechnungsnummer(int rNr) throws RechnungExestiertNichtException;

    public Artikel getArtikel(int aNr) throws ArtikelExestiertNichtException;

    public boolean existiertArtikel(int aNr);

    public boolean istImWarenkorb(Kunde k, int aNr);

    public void inWarenkorbLegen(Artikel a, int kNr) throws KundenNummerExistiertNichtException;

    public void ausWarenkorbEntfernen(Artikel a, int kNr) throws KundenNummerExistiertNichtException;

    public void setBestand(int artNr, int wert, Person p) throws IOException, ArtikelBestandNegativException, ArtikelExestiertNichtException;

    public void setBestellteMenge(int menge,Artikel artikel) throws BestellteMengeEntsprichtNichtderPackungsgroesseException, ArtikelBestellteMengeNegativException, ArtikelBestandZuNiedrigException;

    public void fuegeArtikelEin(String name, String beschreibung, double preis, Mitarbeiter m) throws IOException, ArtikelExestierBereitsException;

    public void fuegeMassengutArtikelEin(String name, String beschreibung, double preis, int packung, Mitarbeiter m ) throws IOException, ArtikelExestierBereitsException;

    public void fuegeKundeEin(String vorname, String nachname, String benutzername, String passwort, String email, String telefon, Adresse adresse) throws IOException, BenutzernameExistiertBereitsException;

    public void fuegeMitarbeiterEin(String vorname, String nachname, String benutzername, String passwort, String email, String telefon, Adresse adresse) throws IOException, MitarbeiterExistiertBereitsException;

    public void rechnungsBestandCheckKaufen(SessionInterface session, int aktuellerKunde) throws IOException, KundenNummerExistiertNichtException, ArtikelBestandNegativException, ArtikelBestandZuNiedrigException, ArtikelExestiertNichtException, RechnungExestiertNichtException;

    public void fuegeRechnungEin(Kunde kunde) throws IOException, RechnungExestiertNichtException;

    public void loescheArtikel(int artNr, Mitarbeiter m) throws IOException, ArtikelExestiertNichtException;

    public void loescheKunde(int kunNr,  Mitarbeiter m) throws IOException, KundenNummerExistiertNichtException;

    public void loescheMitarbeiter(int mNr, Mitarbeiter m) throws IOException, MitarbeiterExistiertNichtException;

    public void loescheRechnung(Rechnung rechnung) throws IOException, RechnungExestiertNichtException;

    public void schreibeArtikel() throws IOException;

    public void schreibeKunden() throws IOException;

    public void schreibeRechung() throws IOException;

    public Rechnung letzteKundenrechnungAusgeben(int kNr) throws IOException, RechnungExestiertNichtException;

    public void schreibeMitarbeiter() throws IOException;

    public Kunde getKunde(int kNr) throws KundenNummerExistiertNichtException;

    public int getKnr(String bName) throws BenutzernameExistiertNichtException;

    public int getMnr(String bName);

    public Mitarbeiter getMitarbeiter(int mNr) throws MitarbeiterExistiertNichtException;

    public Vector<ArtikelBestandsGraph> getArtikelGraph(int daysInPast, String aNr, String name) throws FileNotFoundException, ParseException;

    public Vector<String> printArtikelLog(int daysInPast, String aNr) throws FileNotFoundException, ParseException, KennNummerExistiertNichtException;

    public Vector<String> printArtikelLog(int daysInPast) throws FileNotFoundException, ParseException, KeineEintraegeVorhandenException;

    public Vector<String> printArtikelLog(String aNr) throws FileNotFoundException, ParseException, KennNummerExistiertNichtException;

    public String printArtikelLog() throws FileNotFoundException, KeineEintraegeVorhandenException;

    public Vector<String> printKundenLog(int daysInPast, String kNr) throws FileNotFoundException, ParseException, KennNummerExistiertNichtException;

    public Vector<String> printKundenLog(int daysInPast) throws FileNotFoundException, ParseException, KeineEintraegeVorhandenException;

    public Vector<String> printKundenLog(String kNr) throws FileNotFoundException, ParseException, KennNummerExistiertNichtException;

    public String printKundenLog() throws FileNotFoundException, KeineEintraegeVorhandenException;

    public Vector<String> printMitarbeiterLog(int daysInPast, String mNr) throws FileNotFoundException, ParseException, KennNummerExistiertNichtException;

    public Vector<String> printMitarbeiterLog(int daysInPast) throws FileNotFoundException, ParseException, KeineEintraegeVorhandenException;

    public Vector<String> printMitarbeiterLog(String mNr) throws FileNotFoundException, ParseException, KennNummerExistiertNichtException;

    public String printMitarbeiterLog() throws FileNotFoundException, KeineEintraegeVorhandenException;

    public Vector<String> printRechnungsLog(int daysInPast, String rNr) throws FileNotFoundException, ParseException, KennNummerExistiertNichtException;

    public Vector<String> printRechnungsLog(int daysInPast) throws FileNotFoundException, ParseException, KeineEintraegeVorhandenException;

    public Vector<String> printRechnungsLog(String rNr) throws FileNotFoundException, ParseException, KennNummerExistiertNichtException;

    public String printRechnungsLog() throws FileNotFoundException, KeineEintraegeVorhandenException;

    public void passwortAendern(Person p, String p1, String p2) throws NeuesPasswortFehlerhaftException;

    public void vornamenAendern(Person p, String vorname);

    public void nachnamenAendern(Person p, String nachname);

    public void telefonnummerAendern(Person p, String telefon);

    public void emailAdresseAendern(Person p, String email);

    public void straßeAendern(Person p, String straße);

    public void wohnortAendern(Person p, String ort);

    public void plzAendern(Person p, String plz);
}