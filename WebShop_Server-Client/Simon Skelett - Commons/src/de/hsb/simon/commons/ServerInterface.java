package de.hsb.simon.commons;

import exception.*;
import valueobjects.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Vector;

public interface ServerInterface {

    public SessionInterface login(String username, ClientInterface client);

    public void broadcastNeueArtikelDaten(HashMap<Integer,Artikel> neueDaten);

    public void broadcastNeueKundenDaten(HashMap<Integer,Kunde> neueDaten);

    public void broadcastNeueMitarbeiterDaten(HashMap<Integer,Mitarbeiter> neueDaten);

    public void broadcastNeueRechnungsDaten(HashMap<Integer,Rechnung> neueDaten);

}
