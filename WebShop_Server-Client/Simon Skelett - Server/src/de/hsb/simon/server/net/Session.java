package de.hsb.simon.server.net;

import de.hsb.simon.commons.ClientInterface;
import de.hsb.simon.commons.ServerInterface;
import de.hsb.simon.commons.SessionInterface;
import de.root1.simon.annotation.SimonRemote;
import domain.EShopVerwaltung;
import exception.*;
import valueobjects.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Vector;

@SimonRemote (value = {SessionInterface.class})
public class Session implements SessionInterface{

    private String user;
    private ClientInterface client;
    private ServerInterfaceImpl server;
    private EShopVerwaltung eShop;

    public Session(String user, ClientInterface client, ServerInterfaceImpl server) throws IOException, ClassNotFoundException {
        this.user = user;
        this.client = client;
        this.server = server;
        this.eShop = new EShopVerwaltung("EShop");
    }

    public void unreferenced(){
        server.removeSession(this);
    }

    public void sendMessage(String message){
        server.broadcast(message);
    }

    public ClientInterface getClient(){
        return this.client;
    }

    public Vector gibAlleArtikel() {
        return eShop.gibAlleArtikel();
    }

    public HashMap<Integer,Artikel> gibAlleArtikelHashMapZurueckgeben(){
        return eShop.gibAlleArtikelHashMapZurueckgeben();
    }

    public Vector gibAlleKunden(){
        return eShop.gibAlleKunden();
    }

    public HashMap<Integer,Kunde> gibAlleKundenHashMapZurueckgeben(){
        return eShop.gibAlleKundenHashMapZurueckgeben();
    }

    public Vector gibAlleRechnungen(){
        return eShop.gibAlleRechnungen();
    }

    public HashMap<Integer,Rechnung> gibAlleRechnungenHashMapZurueckgeben() {
        return eShop.gibAlleRechnungenHashMapZurueckgeben();
    }

    public Vector giballeRechnungenEinesKundenZurueckgeben(int kNr) throws RechnungKeineVorhandenException {
        return eShop.giballeRechnungenEinesKundenZurueckgeben(kNr);
    }

    public Vector gibAlleMitarbeiter() {
        return eShop.gibAlleMitarbeiter();
    }

    public HashMap<Integer,Mitarbeiter> gibAlleMitarbeiterHashMapZurueckgeben() {
        return eShop.gibAlleMitarbeiterHashMapZurueckgeben();
    }

    public boolean findeMitarbeiter(String bName, String bPasswort) {
        return eShop.findeMitarbeiter(bName, bPasswort);
    }

    public int getMitarbeiterNr(String benutzername) throws MitarbeiterExistiertNichtException {
        return eShop.getMitarbeiterNr(benutzername);
    }

    public boolean findeKunden(String bName, String bPasswort) {
        return eShop.findeKunden(bName, bPasswort);
    }

    public Vector sucheNachName(String name) {
        return eShop.sucheNachName(name);
    }

    public Rechnung sucheNachRechnungsnummer(int rNr) throws RechnungExestiertNichtException {
        return eShop.sucheNachRechnungsnummer(rNr);
    }

    public Artikel getArtikel(int aNr) throws ArtikelExestiertNichtException {
        return eShop.getArtikel(aNr);
    }

    public boolean existiertArtikel(int aNr) {
        return eShop.existiertArtikel(aNr);
    }

    public boolean istImWarenkorb(Kunde k, int aNr) {
        return eShop.istImWarenkorb(k, aNr);
    }

    public void inWarenkorbLegen(Artikel a, int kNr) throws KundenNummerExistiertNichtException {
        eShop.inWarenkorbLegen(a, kNr);
    }

    public void ausWarenkorbEntfernen(Artikel a, int kNr) throws KundenNummerExistiertNichtException {
        eShop.ausWarenkorbEntfernen(a, kNr);
    }

    public void setBestand(int artNr, int wert, Person p) throws IOException, ArtikelBestandNegativException, ArtikelExestiertNichtException {
        eShop.setBestand(artNr, wert, p);
    }

    public void setBestellteMenge(int menge,Artikel artikel) throws BestellteMengeEntsprichtNichtderPackungsgroesseException, ArtikelBestellteMengeNegativException, ArtikelBestandZuNiedrigException {
        eShop.setBestellteMenge(menge, artikel);
    }

    /**
     * 1. Fügt einen neuen Artikel in den Bestand ein, indem diese Aufgabe an die EShopVerwaltung delegiert wird
     * 2. Sorgt dafür das der Server einen Broadcast versendet, dass Daten geändert wurden
     *
     * @param name -> Name des neuen Artikels
     * @param beschreibung -> Beschreibung des neuen Artikels
     * @param preis -> Preis des neuen Artikels
     * @param m -> Mitarbeiter der den neuen Artikel angelegt hat
     */
    public void fuegeArtikelEin(String name, String beschreibung, double preis, Mitarbeiter m) throws IOException, ArtikelExestierBereitsException {
        eShop.fuegeArtikelEin(name, beschreibung, preis, m);
        HashMap<Integer,Artikel> neueDaten = eShop.gibAlleArtikelHashMapZurueckgeben();
        server.broadcastNeueArtikelDaten(neueDaten);
    }

    public void fuegeMassengutArtikelEin(String name, String beschreibung, double preis, int packung, Mitarbeiter m ) throws IOException, ArtikelExestierBereitsException {
        eShop.fuegeMassengutArtikelEin(name, beschreibung, preis, packung, m);
        HashMap<Integer,Artikel> neueDaten = eShop.gibAlleArtikelHashMapZurueckgeben();
        server.broadcastNeueArtikelDaten(neueDaten);
    }

    public void fuegeKundeEin(String vorname, String nachname, String benutzername, String passwort, String email, String telefon, Adresse adresse) throws IOException, BenutzernameExistiertBereitsException {
        eShop.fuegeKundeEin(vorname, nachname, benutzername, passwort, email, telefon, adresse);
        HashMap<Integer,Kunde> neueDaten = eShop.gibAlleKundenHashMapZurueckgeben();
        server.broadcastNeueKundenDaten(neueDaten);
    }

    public void fuegeMitarbeiterEin(String vorname, String nachname, String benutzername, String passwort, String email, String telefon, Adresse adresse) throws IOException, MitarbeiterExistiertBereitsException {
        eShop.fuegeMitarbeiterEin(vorname, nachname, benutzername, passwort, email, telefon, adresse);
        HashMap<Integer, Mitarbeiter> neueDaten = eShop.gibAlleMitarbeiterHashMapZurueckgeben();
        server.broadcastNeueMitarbeiterDaten(neueDaten);
    }
    // @TODO problematisch...
    public void rechnungsBestandCheckKaufen(SessionInterface session, int aktuellerKunde) throws IOException, KundenNummerExistiertNichtException, ArtikelBestandNegativException, ArtikelBestandZuNiedrigException, ArtikelExestiertNichtException, RechnungExestiertNichtException {
        eShop.rechnungsBestandCheckKaufen(eShop, aktuellerKunde);
    }

    public void fuegeRechnungEin(Kunde kunde) throws IOException, RechnungExestiertNichtException {
        eShop.fuegeRechnungEin(kunde);
        HashMap<Integer, Rechnung> neueDaten = eShop.gibAlleRechnungenHashMapZurueckgeben();
        server.broadcastNeueRechnungsDaten(neueDaten);
    }

    public void loescheArtikel(int artNr, Mitarbeiter m) throws IOException, ArtikelExestiertNichtException {
        eShop.loescheArtikel(artNr, m);
    }

    public void loescheKunde(int kunNr,  Mitarbeiter m) throws IOException, KundenNummerExistiertNichtException {
        eShop.loescheKunde(kunNr, m);
    }

    public void loescheMitarbeiter(int mNr, Mitarbeiter m) throws IOException, MitarbeiterExistiertNichtException {
        eShop.loescheMitarbeiter(mNr, m);
    }

    public void loescheRechnung(Rechnung rechnung) throws IOException, RechnungExestiertNichtException {
        eShop.loescheRechnung(rechnung);
    }

    public void schreibeArtikel() throws IOException {
        eShop.schreibeArtikel();
    }

    public void schreibeKunden() throws IOException {
        eShop.schreibeKunden();
    }

    public void schreibeRechung() throws IOException {
        eShop.schreibeRechung();
    }

    public Rechnung letzteKundenrechnungAusgeben(int kNr) throws IOException, RechnungExestiertNichtException {
        return eShop.letzteKundenrechnungAusgeben(kNr);
    }

    public void schreibeMitarbeiter() throws IOException {
        eShop.schreibeMitarbeiter();
    }

    public Kunde getKunde(int kNr) throws KundenNummerExistiertNichtException {
        return eShop.getKunde(kNr);
    }

    public int getKnr(String bName) throws BenutzernameExistiertNichtException {
        return  eShop.getKnr(bName);
    }

    public int getMnr(String bName) {
        return eShop.getMnr(bName);
    }

    public Mitarbeiter getMitarbeiter(int mNr) throws MitarbeiterExistiertNichtException {
        return eShop.getMitarbeiter(mNr);
    }

    public Vector<ArtikelBestandsGraph> getArtikelGraph(int daysInPast, String aNr, String name) throws FileNotFoundException, ParseException {
        return eShop.getArtikelGraph(daysInPast, aNr, name);
    }

    public Vector<String> printArtikelLog(int daysInPast, String aNr) throws FileNotFoundException, ParseException, KennNummerExistiertNichtException {
        return eShop.printArtikelLog(daysInPast, aNr);
    }

    public Vector<String> printArtikelLog(int daysInPast) throws FileNotFoundException, ParseException, KeineEintraegeVorhandenException {
        return eShop.printArtikelLog(daysInPast);
    }

    public Vector<String> printArtikelLog(String aNr) throws FileNotFoundException, ParseException, KennNummerExistiertNichtException {
        return eShop.printArtikelLog(aNr);
    }

    public String printArtikelLog() throws FileNotFoundException, KeineEintraegeVorhandenException {
        return eShop.printArtikelLog();
    }

    public Vector<String> printKundenLog(int daysInPast, String kNr) throws FileNotFoundException, ParseException, KennNummerExistiertNichtException {
        return eShop.printKundenLog(daysInPast, kNr);
    }

    public Vector<String> printKundenLog(int daysInPast) throws FileNotFoundException, ParseException, KeineEintraegeVorhandenException {
        return  eShop.printKundenLog(daysInPast);
    }

    public Vector<String> printKundenLog(String kNr) throws FileNotFoundException, ParseException, KennNummerExistiertNichtException {
        return eShop.printKundenLog(kNr);
    }

    public String printKundenLog() throws FileNotFoundException, KeineEintraegeVorhandenException {
        return  eShop.printKundenLog();
    }

    public Vector<String> printMitarbeiterLog(int daysInPast, String mNr) throws FileNotFoundException, ParseException, KennNummerExistiertNichtException {
        return eShop.printMitarbeiterLog(daysInPast, mNr);
    }

    public Vector<String> printMitarbeiterLog(int daysInPast) throws FileNotFoundException, ParseException, KeineEintraegeVorhandenException {
        return eShop.printMitarbeiterLog(daysInPast);
    }

    public Vector<String> printMitarbeiterLog(String mNr) throws FileNotFoundException, ParseException, KennNummerExistiertNichtException {
        return eShop.printMitarbeiterLog(mNr);
    }

    public String printMitarbeiterLog() throws FileNotFoundException, KeineEintraegeVorhandenException {
        return eShop.printMitarbeiterLog();
    }

    public Vector<String> printRechnungsLog(int daysInPast, String rNr) throws FileNotFoundException, ParseException, KennNummerExistiertNichtException {
        return eShop.printRechnungsLog(daysInPast, rNr);
    }

    public Vector<String> printRechnungsLog(int daysInPast) throws FileNotFoundException, ParseException, KeineEintraegeVorhandenException {
        return eShop.printRechnungsLog(daysInPast);
    }

    public Vector<String> printRechnungsLog(String rNr) throws FileNotFoundException, ParseException, KennNummerExistiertNichtException {
        return eShop.printRechnungsLog(rNr);
    }

    public String printRechnungsLog() throws FileNotFoundException, KeineEintraegeVorhandenException {
        return eShop.printRechnungsLog();
    }

    public void passwortAendern(Person p, String p1, String p2) throws NeuesPasswortFehlerhaftException {
        eShop.passwortAendern(p, p1, p2);
    }

    public void vornamenAendern(Person p, String vorname) {
        eShop.vornamenAendern(p, vorname);
    }

    public void nachnamenAendern(Person p, String nachname) {
        eShop.nachnamenAendern(p, nachname);
    }

    public void telefonnummerAendern(Person p, String telefon) {
        eShop.telefonnummerAendern(p, telefon);
    }

    public void emailAdresseAendern(Person p, String email) {
        eShop.emailAdresseAendern(p, email);
    }

    public void straßeAendern(Person p, String straße) {
        eShop.straßeAendern(p, straße);
    }

    public void wohnortAendern(Person p, String ort) {
        eShop.wohnortAendern(p, ort);
    }

    public void plzAendern(Person p, String plz) {
        eShop.plzAendern(p, plz);
    }

}
