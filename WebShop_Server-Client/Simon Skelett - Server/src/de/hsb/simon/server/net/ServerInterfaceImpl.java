package de.hsb.simon.server.net;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.hsb.simon.commons.ClientInterface;
import de.hsb.simon.commons.ServerInterface;
import de.hsb.simon.commons.SessionInterface;
import de.root1.simon.Registry;
import de.root1.simon.Simon;
import de.root1.simon.annotation.SimonRemote;
import de.root1.simon.exceptions.NameBindingException;
import valueobjects.Artikel;
import valueobjects.Kunde;
import valueobjects.Mitarbeiter;
import valueobjects.Rechnung;

@SimonRemote(value = {ServerInterface.class})

public class ServerInterfaceImpl implements ServerInterface{

    private Registry registry;
    private List<SessionInterface> sessions;

    public ServerInterfaceImpl() {
        sessions = new ArrayList<SessionInterface>();
    }

    public void startServer() throws IOException, NameBindingException{
        registry = Simon.createRegistry(4753);

        registry.bind("myServer", this);
    }

    public void stopServer(){

        registry.unbind("myServer");

        registry.stop();
    }

    public SessionInterface login(String user, ClientInterface client) {
        try {
            Session session = new Session(user, client, this);

            sessions.add(session);

            client.callback("Hallo!");

            return  session;

        } catch (IOException io) {

        } catch (ClassNotFoundException cnf) {

        }

        return null;
    }

    public void broadcast(String text){

        for(int i = 0; i < sessions.size(); i++){
            sessions.get(i).getClient().callback(text);
        }
    }

    /**
     * Übermittelt allen Sessions(und damit allen verbundenen Clients) das Daten geändert wurden
     *
     * @param neueDaten -> Die neuen Daten für die Hashmap mit sämtlichen Artikeln
     */
    public void broadcastNeueArtikelDaten(HashMap<Integer,Artikel> neueDaten) {
        for(int i = 0; i < sessions.size(); i++){
            sessions.get(i).getClient().receiveNeueArtikelDaten(neueDaten);
        }
    }

    public void broadcastNeueKundenDaten(HashMap<Integer, Kunde> neueDaten){
        for(int i = 0; i < sessions.size(); i++){
            sessions.get(i).getClient().receiveNeueKundenDaten(neueDaten);
        }
    }

    public void broadcastNeueMitarbeiterDaten(HashMap<Integer, Mitarbeiter> neueDaten){
        for(int i = 0; i < sessions.size(); i++){
            sessions.get(i).getClient().receiveNeueMitarbeiterDaten(neueDaten);
        }
    }

    public void broadcastNeueRechnungsDaten(HashMap<Integer, Rechnung> neueDaten){
        for(int i = 0; i < sessions.size(); i++){
            sessions.get(i).getClient().receiveNeueRechnungsDaten(neueDaten);
        }
    }

    public void removeSession(SessionInterface session){
        sessions.remove(session);
        this.broadcast("A user logged out.");
    }
}
